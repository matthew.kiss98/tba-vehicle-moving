/*
    author: Kiss Mate
*/

package tba.vehicles.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import tba.vehicles.backend.utils.Coordinate;

/**
 * Main model in Vehicle Simulator App storing the vehicle's ID and a geographical coordinate as its destination.
 */
public class Vehicle {
    private static int autoIncrement = 1;
    private int id;
    private Coordinate destination;

    public Vehicle() {
        id = autoIncrement++;
    }

    public Vehicle(@JsonProperty("dest") Coordinate destination) {
        id = autoIncrement++;
        this.destination = destination;
    }

    public Coordinate getDestination() {
        return destination;
    }

    public void setDestination(Coordinate destination) {
        this.destination = destination;
    }

    public int getVehicleId() {
        return id;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", destination=" + destination +
                '}';
    }
}
