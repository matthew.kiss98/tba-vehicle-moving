package tba.vehicles.backend.dao;

import tba.vehicles.backend.model.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleDao {

    List<Vehicle> selectAllVehicles();
    Optional<Vehicle> selectVehicleById(int id);
    int insertVehicle(Vehicle vehicle);
    int updateVehicleById(int id, Vehicle vehicle);
    int deleteVehicleById(int id);
    int deleteAllVehicles();
}
