/*
    author: Kiss Mate
*/

package tba.vehicles.backend.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Repository;
import tba.vehicles.backend.model.Vehicle;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Vehicle data access service with an in-memory database.
 */
@Repository("inMemoryDao")
public class VehicleDataAccessService implements VehicleDao {

    private static List<Vehicle> DB = new ArrayList<>();

    @Override
    public List<Vehicle> selectAllVehicles() {
        return DB;
    }

    /**
     * Finds and returns a vehicle with given ID, if exists.
     * @param id ID of the vehicle searched for.
     * @return Returns an optional containing the vehicle with given ID, if exists.
     */
    @Override
    public Optional<Vehicle> selectVehicleById(@JsonProperty("id") int id) {
        return DB.stream()
                .filter(vehicle -> vehicle.getVehicleId() == id)
                .findFirst();
    }

    @Override
    public int insertVehicle(Vehicle vehicle) {
        if (vehicle != null) {
            DB.add(vehicle);
        } else {
            DB.add(new Vehicle());
        }
        return 1;
    }

    @Override
    public int updateVehicleById(@JsonProperty("id") int id, Vehicle newVehicle) {
        Optional<Vehicle> vehicleToUpdate = selectVehicleById(id);

        if (vehicleToUpdate.isPresent()) {
            int indexOfVehicleToUpdate = DB.indexOf(vehicleToUpdate.get());

            if (newVehicle.getDestination() != null) {
                DB.get(indexOfVehicleToUpdate).setDestination(newVehicle.getDestination());
            }
            return 1;
        }
        return 0;
    }

    @Override
    public int deleteVehicleById(@JsonProperty("id") int id) {
        Optional<Vehicle> vehicleToDelete = selectVehicleById(id);

        if (vehicleToDelete.isPresent()) {
            DB.remove(vehicleToDelete.get());
            return 1;
        }
        return 0;
    }

    @Override
    public int deleteAllVehicles() {
        if (DB.isEmpty()) {
            return 0;
        }
        else {
            DB.clear();
            return 1;
        }
    }
}
