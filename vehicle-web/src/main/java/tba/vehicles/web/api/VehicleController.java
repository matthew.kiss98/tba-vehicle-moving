/*
    author: Kiss Mate
*/

package tba.vehicles.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tba.vehicles.backend.model.Vehicle;
import tba.vehicles.web.service.VehicleService;

import java.util.List;

/**
 * Spring REST controller handling CRUD resources.
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value="vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping
    public List<Vehicle> getAllVehicles() {
        return vehicleService.getAllVehicles();
    }

    @GetMapping(path = "{id}")
    public Vehicle getVehicleById(@PathVariable("id") int id) {
        return vehicleService.getVehicleById(id).orElse(null);
    }

    @PostMapping
    public void addVehicle(@RequestBody(required = false) Vehicle vehicle) {
        vehicleService.addVehicle(vehicle);
    }

    @PostMapping(value = "multiple")
    public void addVehicles(@RequestBody(required = false) List<Vehicle> vehicles) {
        vehicles.forEach(vehicleService::addVehicle);
    }

    @PutMapping(path = "{id}")
    public void updateVehicle(@PathVariable("id") int id, @RequestBody Vehicle vehicleToUpdate) {
        vehicleService.updateVehicle(id, vehicleToUpdate);
    }

    @DeleteMapping
    public int deleteAllVehicles() {
        return vehicleService.deleteAllVehicles();
    }

    @DeleteMapping(path = "{id}")
    public void deleteVehicle(@PathVariable("id") int id) {
        vehicleService.deleteVehicle(id);
    }
}
