/*
    author: Kiss Mate
*/

package tba.vehicles.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import tba.vehicles.backend.dao.VehicleDao;
import tba.vehicles.backend.model.Vehicle;

import java.util.List;
import java.util.Optional;

/**
 * Implements business logic for Vehicle operations.
 */
@Service
public class VehicleService {

    private final VehicleDao vehicleDao;

    @Autowired
    public VehicleService(@Qualifier("inMemoryDao") VehicleDao vehicleDao) {
        this.vehicleDao = vehicleDao;
    }

    public List<Vehicle> getAllVehicles() {
        return vehicleDao.selectAllVehicles();
    }

    public Optional<Vehicle> getVehicleById(int id) {
        return vehicleDao.selectVehicleById(id);
    }

    public int addVehicle(Vehicle vehicle) {
        return vehicleDao.insertVehicle(vehicle);
    }

    public int updateVehicle(int id, Vehicle newVehicle) {
        return vehicleDao.updateVehicleById(id, newVehicle);
    }

    public int deleteVehicle(int id) {
        return vehicleDao.deleteVehicleById(id);
    }

    public int deleteAllVehicles() { return vehicleDao.deleteAllVehicles();}
}
